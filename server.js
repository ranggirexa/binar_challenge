require('dotenv').config()
const client = require('./databasepg.js')
const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();
const bcrypt = require('bcrypt')

app.listen(3300, ()=>{
    console.log("Sever is now listening at port 3300");
})

client.connect();
app.use(express.json())




app.get('/posts', authenticationToken, (req, res) =>{
    res.json({
        status:200,
        message:'success'
    })
})

// app.get('/users', (req, res) => {
//     res.json(users)
// })

// app.post('/user', (req, res) =>{
//     const user = {
//         name:res.body.username, 
//         password:req.body.password, 
//         role:req.body.role}
//     users.push(user)
//     res.status(201).send()
// })

// app.post('/login', (req, res)=> {
//     // const user = req.body;
//     // let insertQuery = `insert into users(id, username, password, role) 
//     //                    values(${user.id}, '${user.username}', '${user.password}', '${user.role}')`

//     // client.query(insertQuery, (err, result)=>{
//     //     if(!err){
//     //         res.send('Insertion was successful')
//     //     }
//     //     else{ console.log(err.message) }
//     // })

//     // client.end;

//     const username = {name: req.body.username}
//     const user = {name: username}

//     const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
//     res.json({accessToken: accessToken})

// })

app.post('/register', async (req, res) => {
    const check_username = await client.query(`Select * from users where username='${req.body.username}'`);

    if (check_username.rows.length > 0) {
        res.json({
            status:500,
            message:'failed, username was taken',
        })
    } else {
        try {
            const hashedPassword = await bcrypt.hash(req.body.password, 10)
            let insertQuery = `insert into users(username, password, role) 
                        values('${req.body.username}', '${hashedPassword}', '${req.body.role}')`

            if(client.query(insertQuery)) {
            // res.status(200).send;
                res.json({
                    status:200,
                    message:'success insert data',
                    data:[
                        {
                            username:req.body.username,
                            password:req.body.password,
                            role:req.body.role,
                        }
                    ]
                })
            } else {
                res.json({
                    status:404,
                    message:'failed insert data'
                })
            }
        } catch {
        res.status(500).send()
        }
    }
})

app.post('/login', async (req, res) => {
    arr_user = []
    token = []

    const data = await client.query(`Select * from users where username='${req.body.username}'`);

    if (data.rows.length == 0) {
        return res.status(400).send('Cannot find user')
    } else{
        const username = {name: req.body.username}
        const user = {name: username}
    
        const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
        // res.json({accessToken: accessToken})
        token.push(accessToken)
        arr_user.push(data.rows[0]);
    }

    try {
        if(await bcrypt.compare(req.body.password, arr_user[0].password)) {
            // res.status(200).send;
            res.json({
                status:200,
                message:'success',
                access_token:token[0],
                response:[
                    data.rows[0]
                ]
            })
        } else {
            res.json({
                status:501,
                message:'failed',
                response:[
                    'user not found'
                ]
            })
        }
    } catch {
        res.json({
            status:404,
            message:'failed',
            response:[
                'something wrong'
            ]
        })
    }
   
})

function authenticationToken(req, res, next) {
    const authHeder = req.headers['authorization']
    const token =  authHeder && authHeder.split(' ')[1]
    if (token == null) return res.sendStatus(401) 

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403)   
        req.user = user
        next()
    })
}

// app.listen(3000)